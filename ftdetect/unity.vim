au BufRead,BufNewFile *.mat            set filetype=yaml
au BufRead,BufNewFile *.meta           set filetype=yaml
au BufRead,BufNewFile *.unity           set filetype=yaml
