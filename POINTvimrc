" Turn off vi compatibility
set nocompatible

" Pour avoir des couleurs lisibles sur fond noir (pas de bleu super foncé).
:colorscheme elflord

" Pour ouvrir automatiquement les ballots.
filetype off
call pathogen#infect()
call pathogen#helptags()

" Correction orthographe/grammaticale.
" Pour utiliser aspell.
:hi SpellBad ctermfg=0 ctermbg=7<CR>
map <F7> :set nospell<CR>
map <F8> :set spell spelllang=fr<CR>
map <F9> :set spell spelllang=en<CR>
let g:languagetool_jar='$HOME/bin/LanguageTool-2.1/languagetool-commandline.jar'
let g:languagetool_disable_rules='WHITESPACE_RULE,COMMA_PARENTHESIS_WHITESPACE,CURRENCY,HUNSPELL_NO_SUGGEST_RULE,FRENCH_WHITESPACE,UNPAIRED_BRACKETS'
nmap ,a :!echo -e "\n Correction en anglais en cours *attendre* que l'écran normal s'affiche\n"<CR>:!correction_grammaticale_anglais.sh %<CR>:sp %.LTcheck<CR>gg/Line<CR>w:let numligne = expand("<cword>")<CR>/column<CR>w:let numcolonne = expand("<cword>")<CR>}:wincmd j<CR>:execute ":"numligne<CR>:execute "normal 0"expand(numcolonne)"l"<CR>
nmap ,c :!echo -e "\n Correction en français en cours *attendre* que l'écran normal s'affiche\n"<CR>:!correction_grammaticale.sh %<CR>:sp %.LTcheck<CR>gg/Line<CR>w:let numligne = expand("<cword>")<CR>/column<CR>w:let numcolonne = expand("<cword>")<CR>}:wincmd j<CR>:execute ":"numligne<CR>:execute "normal 0"expand(numcolonne)"l"<CR>
nmap ,n :wincmd k<CR>/Line<CR>w:let numligne = expand("<cword>")<CR>/column<CR>w:let numcolonne = expand("<cword>")<CR>}:wincmd j<CR>:execute ":"numligne<CR>:execute "normal 0"expand(numcolonne)"l"<CR>
nmap ,p :!echo -e "\n Correction en cours *attendre* que l'écran normal s'affiche\n"<CR>:!echo %<CR>

" La coloration syntaxique.
color ron
syntax on
filetype on
filetype indent plugin on

set encoding=utf-8
set fileencoding=utf-8

" Le plugin pep8 valide un peu le code (juste python?).
let g:pep8_map='<leader>8'

" La commande d'indentation, plus de raison de se plaindre.
function! CppNoNamespaceAndTemplateIndent()
    let l:cline_num = line('.')
    let l:cline = getline(l:cline_num)
    let l:pline_num = prevnonblank(l:cline_num - 1)
    let l:pline = getline(l:pline_num)
    while l:pline =~# '\(^\s*{\s*\|^\s*//\|^\s*/\*\|\*/\s*$\)'
        let l:pline_num = prevnonblank(l:pline_num - 1)
        let l:pline = getline(l:pline_num)
    endwhile
    let l:retv = cindent('.')
    let l:pindent = indent(l:pline_num)
    if l:cline =~# '^\s*public\s*:\s*$'
        let l:retv = 0
    elseif l:pline =~# '^\s*template\s*<.*$'
        let l:retv = l:pindent
    elseif l:pline =~# '\s*typename\s*.*,\s*$'
        let l:retv = l:pindent
    elseif l:cline =~# '^\s*>\s*$'
        let l:retv = l:pindent - &shiftwidth
    elseif l:pline =~# '\s*typename\s*.*>\s*$'
        let l:retv = l:pindent - &shiftwidth
    elseif l:pline =~# '^\s*namespace.*'
        let l:retv = 0
    endif
    return l:retv
endfunction

if has("autocmd")
    autocmd BufEnter *.{cc,cxx,cpp,h,hh,hpp,hxx} setlocal indentexpr=CppNoNamespaceAndTemplateIndent()
endif

" Mes favoris d'indentation.
set autoindent
set cindent
set cinkeys=0{,0},0),:,!^I,o,O,e,;,#
set indentkeys=0{,0},0),:,!^I,o,O,e,*<Return>,=?>,=<?,=*/

set path=.,/usr/include,
set path+=/usr/local/include
set path+=/usr/include/c++/5

" alt-gauche et alt-droit pour changer de buffer.
map <M-Left> :bp<CR>
map <M-Right> :bn<CR>

" alt-haut et alt-bas pour passer d'une erreur à l'autre à la compilation.
map <M-Up> :cp<CR>
map <M-Down> :cn<CR>

" Pour pouvoir coller avec la sourie sans bug d'indentation ou de lignes
" commentées.
map <F3> :set paste<CR>
map <F4> :set nopaste<CR>

" Affiche la parenthèse correspondant
set sm
set incsearch           " recherche incrémentale
set hlsearch            " "highlighte" le dernier motif de recherche

" vim-fugitive
" Pour le support git dans vim
:set diffopt+=vertical

" Options pour YouCompleteMe
let g:ycm_extra_conf_globlist = ['~/Sources/git/xml2xsd/*']
map <c-a> :YcmCompleter GetType<CR>
map <c-}> :YcmCompleter GoToDeclaration<CR>
map <c-]> :YcmCompleter GoToDefinition<CR>
let g:ycm_global_ycm_extra_conf = '~/.vim/global.ycm_extra_conf.py'

" Afficher les espaces non voulus.
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Pour python
au FileType python setlocal tabstop=8 expandtab shiftwidth=4 softtabstop=4

" Pour empêcher que vi copie directement dans le tampon de la sourie.
set clipboard-=autoselect

" Pour retrouver ma ligne lorsque je réouvre un fichier.
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

let ruby_space_errors = 1

" vimvs (le contraire de vsvim)
map <F6> :w<CR>:make -j4<CR>
map <F12> :YcmCompleter GoTo<CR>

" Pour éviter d'avoir des changements inutile à la dernière ligne des fichiers que d'autres logiciels vont ensuite défaire.
set nofixendofline

" Pour ne pas me faire bloqué vim à chaque fois que UnityPlayer.log est mis à
" jour.
set noautoread

" Cursor in terminal
" https://vim.fandom.com/wiki/Configuring_the_cursor
" 1 or 0 -> blinking block
" 2 solid block
" 3 -> blinking underscore
" 4 solid underscore
" Recent versions of xterm (282 or above) also support
" 5 -> blinking vertical bar
" 6 -> solid vertical bar

if &term =~ '^xterm'
        " normal mode
        let &t_EI .= "\<Esc>[2 q"
        " insert mode
        let &t_SI .= "\<Esc>[6 q"
endif
