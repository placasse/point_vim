#!/bin/bash
#===============================================================================
#
#          FILE:  correct_grammaire.sh
#
#         USAGE:  ./correction_gramaticale.sh
#
#   DESCRIPTION:  utilise languagetool dans bin pour avoir un correcteur
# 		grammatical utilisable sur un fichier latex
#
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   Corentin Barbu(),
#       COMPANY:
#       VERSION:  1.0
#       CREATED:  17/10/2009 14:38:59 CEST
#      REVISION:  ---
#===============================================================================
#
# Voir bin/correction_grammaticale.sh pour de l'aide
#

sed -e 's/\$[^$]\+\$/   /g' -e 's/\\cite{[^}]\+}/        /g' -e 's/\\[a-zA-Z_]\+/  /g' -e 's/~/ /g' $1 > $1.sanslatex
java -jar ~/bin/LanguageTool-2.6/languagetool-commandline.jar --disable WHITESPACE_RULE,COMMA_PARENTHESIS_WHITESPACE,FRENCH_WHITESPACE,HUNSPELL_NO_SUGGEST_RULE,UNPAIRED_BRACKETS,UPPERCASE_SENTENCE_START,CURRENCY,EN_UNPAIRED_BRACKETS --language en-US $1.sanslatex > $1.LTcheck

exit 0

