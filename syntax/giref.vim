" Vim syntax file
" Language:	giref
" Maintainer:	Patrick Lacasse <patrick.m.lacasse@gmail.com>
" Last Change:	2009 février
"
" This vim syntax file works on vim 5.6, 5.7, 5.8 and 6.x.
" It implements Bram Moolenaar's April 25, 2001 recommendations to make
" the syntax file maximally portable across different versions of vim.


" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

if !exists("main_syntax")
  let main_syntax = 'giref'
endif

syn match girefIdentifier "[a-zA-Z_][a-zA-Z_0-9]*" contained display

syn keyword girefType chainecar booleen contained

" Les champs analytiques.
syn keyword girefChampsAnalytique scallin scalaire champgeosafe champgeolin champgeoquad vectoriel1d vectoriel2d vectoriel3d to2sym to2nsym to2nsymaconstant to4elasticite  contained
syn keyword girefChampsAnalytique champssurgeom scalsurgeom v3dsurgeom v2dsurgeom to2surgeom to2nsymsurgeom to4surgeom contained
syn keyword girefChampsAnalytique empilementto2 empilementto2nsym empilementto4 empilementv3d empilementv2d contained
syn keyword girefChampsAnalytique empilementto2_discelem empilementto2nsym_discelem empilementto4_discelem empilementv3d_discelem empilementv2d_discelem contained
syn keyword girefCHampsAnalytique scalh_ugn scalh_diese contained

" Les champs pas analytiques.
syn keyword girefChampsDDL scallin scalquad scalhierarchique scalquadbulle scalcrourav scalquadbulleh scalsommetfaceelement scalsommetface3d scalsommetaretefacex3_3d scalqc scalmini scalmini3d scalhermite3dordre3 scalhermite3dordre3Inc scalp0isop1 scalp0isop2 scalp1isop1bulle scalp1isop2 scalp1isop2bulle scalconstelem scalp1discelem scalp1discelemb scalp1discelemc scallinelem scalquadelem scalp1nc scalquadbulle scalquadbulleh contained
syn keyword girefChampsDDL v2dlin v2dp1nc v2dmini v2dp1isop1bulle v2dp1isop2 v2dquad v2dcrourav v2dcubique v2dquartique v2dconstelem v2dlinelem v2dhierarchique v2dquadbulle v2dquadbulleh v2dquadelem contained
syn keyword girefChampsDDL v3dlin v3dp1nc v3dmini v3dp1isop1bulle v3dp1isop2 v3dquad v3dcrourav v3dcubique v3dquartique v3dconstelem v3dlinelem v3dhierarchique v3dquadbulle v3dquadbulleh v3dqc v3dquadelem contained
syn keyword girefChampsDDL to2symlin to2symquad to2symconstelem to2symlinelem to2symquadelem contained
syn keyword girefChampsDDL to2nsymlin to2nsymquad to2nsymconstelem to2nsymlinelem to2nsymquadelem contained

syn keyword girefChampPtsIntegration scalptsintg scalptsintgcontinu v2dptsintg v2dptsintgcontinu v3dptsintg v3dptsintgcontinu to2nsymptsintg to2nsymptsintgcontinu to4symptsintg to4symptsintgcontinu

"
syn keyword girefKeyword newchamp newchampcopie newchampcopieempilementv2d newchampcopieempilementv3d newchampcopieempilementto2sym contained
syn keyword girefInclude include contained
syn keyword girefKeyword if fonction reinterpole alias derivee_partielle cartesien cylindrique changementrepere normalegeometrie gradient_deformation suiveuse_normale critere fctn contained
syn keyword girefFunction exp sin cos log contained
syn keyword girefProprieteMateriaux hookeen hookeen_mixte neohookeen_mixte mooneyrivlin mooneyrivlin_mixte yeoh_mixte melange_matmecsol_mixte contained
syn keyword girefSpecialVar formulation_axisymetrique formulation_2d normale_plan GIREF_COMPILE_EXPALG GIREF_MAPLE_OPTIMISE_EXPALG contained

" Descripteur de déformation
syn keyword girefType descripteur_deformation contained
syn keyword girefSpecialVar descr_u contained
syn keyword girefKeyword depl= grad_def= contained

" Les schémas d'intégrations.
syn keyword girefType schemaintg contained
syn keyword girefSpecialVar schema_newton_cotes_arete schema_newton_cotes_element1d schema_newton_cotes_face3 schema_newton_cotes_face4 schema_newton_cotes_triangle schema_newton_cotes_quadrangle schema_newton_cotes_tetra schema_newton_cotes_hexa schema_newton_cotes_prismetri contained
syn keyword girefSpecialVar schema_grille_arete schema_grille_element1d schema_grille_face3 schema_grille_face4 schema_grille_triangle schema_grille_quadrangle schema_grille_tetra schema_grille_hexa schema_grille_prismetri contained
syn keyword girefSpecialVar schema_gaussleg_projette_arete schema_gaussleg_projette_element1d schema_gaussleg_projette_face3 schema_gaussleg_projette_face4 schema_gaussleg_projette_triangle schema_gaussleg_projette_quadrangle schema_gaussleg_projette_tetra schema_gaussleg_projette_hexa schema_gaussleg_projette_prismetri contained
syn keyword girefSpecialVar ptintgarete ptintgelement1d ptintgface3 ptintgface4 ptintgtriangle ptintgquadrangle ptintgtetra ptintghexa ptintgprismetri_legendre ptintgprismetri_triangle contained
syn keyword girefSpecialVar schemap0isop2 schemap1isop2 schemap1isop1bulle schemap1isop2bulle schemap1isop23d schemap1isop2NC contained

" autres mots clés
syn keyword girefType changement_repere_123XEZXYZ normalechampgeometrique normalegeometrie contained
syn keyword girefType problemeef ProblemeEF
syn keyword girefType ccninfcorr cc_norme_infinie_correction

"Formulations
syn keyword girefType formulation_diffusion
syn keyword girefType formulation_diffusion_source
syn keyword girefType formulation_diffusion_source_inst
syn keyword girefType formulation_diffusion_inst
syn keyword girefType formulation_diffusion_supg
syn keyword girefType formulation_diffusion_source_supg
syn keyword girefType formulation_diffusion_source_inst_supg
syn keyword girefType formulation_diffusion_inst_supg
syn keyword girefType formulation_transport_diffusion_source_inst
syn keyword girefType formulation_transport_diffusion_inst
syn keyword girefType formulation_transport_diffusion_source_inst_supg
syn keyword girefType formulation_transport_diffusion_inst_supg
syn keyword girefType formulation_transport_diffusion_source_inst
syn keyword girefType formulation_transport_diffusion_inst
syn keyword girefType formulation_transport_diffusion_source_inst_supg
syn keyword girefType formulation_transport_diffusion_inst_supg
syn keyword girefType formulation_stokes
syn keyword girefType formulation_stokes_supg
syn keyword girefType formulation_navier_stokes_inst
syn keyword girefType formulation_navier_stokes_stat
syn keyword girefType formulation_navier_stokes_inst_supg
syn keyword girefType formulation_navier_stokes_stat_supg
syn keyword girefType formulation_terme_source
syn keyword girefType formulation_terme_source_supg
syn keyword girefType formulation_transport_convectif
syn keyword girefType formulation_transport_convectif_supg

"Les PPs
syn keyword girefKeyword ajoutepreposttraitement ajoutePrePostTraitement
syn keyword girefType pp_calcul_trainee
syn keyword girefType pp_resolution_probleme
syn keyword girefType AvantPremierePeriode
syn keyword girefType AvantPremierPasTemps
syn keyword girefType DebutPasDeTemps
syn keyword girefType DebutBoucleNlin
syn keyword girefType FinAssemblageDomaine
syn keyword girefType FinAssemblage
syn keyword girefType FinMatVecElementaire
syn keyword girefType DebutIterationNlin
syn keyword girefType FinIterationLS
syn keyword girefType FinLS
syn keyword girefType FinIterationNlin
syn keyword girefType FinBoucleNlin
syn keyword girefType FinPasDeTemps

" Les solveurs
syn keyword girefKeyword options_prefixees_petsc
syn keyword girefKeyword solveur_lin_petsc solveur_nlin_petsc solveur_inst_nlin_petsc solveur_hp contained
syn keyword girefKeyword solveurlinpetsc solveurnlinpetsc solveurinstnlinpetsc solveurhp contained
syn keyword girefArgumentSolveur direct gradient_conjugue gmres gcr bcgs bicg minres lsqr richardson chebyshev cqmr cgs tfqmr pcr precond_seulement qcg fgmres mlq_symetrique contained
syn keyword girefArgumentSolveur aucun defaut vrai valeur_singuliere usager contained
syn keyword girefArgumentSolveur aucun jacobi sor lu bjacobi mg eisenstat ilu icc asm sles composite redondant spai milu nn cholesky usager ramg lsc fieldsplit mat gcr_mefpp contained
syn keyword girefArgumentSolveur quasi_statique euler_implicit crank_nicholson galerkin euler_explicit gear theta premier_pas_connu newmark contained
syn keyword girefArgumentSolveur type_solveur type_precond fieldsplit_type tolerance prefixe_options config_options_sous_bloc contained
syn keyword girefArgumentSolveur additif multiplicatif symetrique_multiplicatif schur contained
syn keyword girefArgumentSolveur direct_petsc direct_bcs direct_mumps direct_superlu direct_superlu_dist contained
syn keyword girefArgumentSolveur schema_temps synchronise_sur_maitre solveur_lin parametres_nlin controle_residu_avant_assemblage reassemblages_a_faire reassemblagesafaire verbose contained
syn keyword girefArgumentSolveur parametres_matrice_masse parametresmatricemasse parametres_matrice_rigidite parametresmatricerigidite solveur_lin_residu_modifie contained
syn keyword girefArgumentSolveur reinterpole_champs_T0 reinterpole_champs_T1 reinitialisation_residus contained
syn keyword girefArgumentSolveur type_matrice typematrice csrsym csr contained
syn keyword girefArgumentSolveur suivi_convergence suiviconvergence contained

" Conditions aux limites
syn keyword girefConditionLimite dirichlet neumann robin neumannnonlin pression contained

" Booléen
syn keyword girefBoolean false true True vrai Vrai false False faux Faux f t v contained
" Ces lettres ne représente pas toujours vrai ou faux, elles entrent souvent
" en conflit avec les variables des champs scalaires algèbriques fonctions.
"syn match   girefBoolean "[fFtTvV]\(.*]\)\@=" contained

" Entité réservée
syn keyword girefEntiteReservee Peau contained

syn match   girefKeyword "f(\@=" contained

" Pour les .enti et .mail et .geom
syn keyword girefKeyword Version ListEntitesGeometrique EntiteGeometrique Version Points points Courbes courbes Surfaces surfaces Volumes volumes Geometrie Maillage

" Le namespace exportation
syn keyword girefKeyword ajouteChamp ajouteChampCommeAutreChamp ajouteChampCommeChampPPI ajouteChampProjectionL2 ajouteDReel ajoutePiloteExportation ajouteTO2NSym ajouteTO2Sym ajouteTO4Sym ajouteV2D ajouteV3D
syn keyword girefKeyword asgnAjouteExtensionAuxNomsFichiersDesChamps asgnDegreInterpolation asgnDimension asgnExporteMaillage asgnFacteurEchelle asgnNomObjetVu asgnUnChampParFichier
syn keyword girefKeyword Exportation exportation exporteSeulementCommeChampPPI exporteTousCommeChampPrecalculePtsIntegration
syn keyword girefKeyword forceMiseAJourMaillage format_exportation frequence_exportation
syn keyword girefKeyword prefixe_exportation
syn keyword girefKeyword asgnExporteToujoursMaillage
syn keyword girefKeyword ajouteCC
syn keyword girefArgumentExportation Amdba BinXDR Fluent GIREF Gmsh Mosaic Patran PLY VTK VU eMAILLAGE_LINEAIRE eMAILLAGE_QUADRATIQUE

" Le namespace Actions:
syn keyword girefKeyword Actions actions resoud execute

syn match girefOperator     "[<>]" contained display
syn match girefOperator     "[<>=]=" contained display
syn match girefOperator     "[+-]" contained
syn match girefOperator     "\^" contained
syn match girefOperator     "/" contained
syn match girefError        "\\" contained
syn match girefOperator     "\\$" contained
syn match girefOperator     "\*" contained
syn match girefComment      "#.*" display
syn match girefComment      "//.*" display
syn match girefString       "\"[^\"]*\"" contained display

" Colorie 1.11e+3 1.11e-3 1.11e+3 1.e+1
syn match girefFloat        "\<[+-]\?\d\.\?\d*e[+-]\?\d\+\>" contained display
" Colorie 11.11 +11.11 -11.11 .11 +.11 -.11 11 +11 -11
syn match girefFloat        "\<[+-]\?\d*\(\.\d\+\)\?\>" contained display
" Colorie 11. +11. -11.
syn match girefFloat        "\<[+-]\?\d\+\." contained display

syn match girefParentOpenError        "[[{(]" contained display
syn match girefParentCloseError        "[)}\]]" contained display

syn region girefParent       matchgroup=Delimiter start="{" end="}" contained contains=@girefClInside
syn region girefParent       matchgroup=Delimiter start="(" end=")" contained contains=@girefClInside
syn region girefParent       matchgroup=Delimiter start="\[" end="\]" contained contains=@girefClInside

syn cluster girefClInside contains=girefIdentifier,girefType,girefChampsAnalytique,girefChampsDDL,girefChampPtsIntegration,girefInclude,girefKeyword,girefFunction,girefProprieteMateriaux,girefSpecialVar,girefArgumentSolveur,girefConditionLimite,girefBoolean,girefComment,girefString,girefFloat,girefOperator,girefParent,girefParentOpenError,girefParentCloseError,girefArgumentExportation,girefEntiteReservee

syn match girefTop "" contains=@girefClInside

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_giref_syn_inits")
  if version < 508
    let did_giref_syn_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink girefType                Type
  HiLink girefChampsAnalytique    Type
  HiLink girefChampsDDL           Type
  HiLink girefChampPtsIntegration Type
  HiLink girefInclude             Statement
  HiLink girefKeyword             Statement
  HiLink girefFunction            Statement
  HiLink girefProprieteMateriaux  Statement
  HiLink girefComment             Comment
  HiLink girefString              String
  HiLink girefBoolean             Boolean
  HiLink girefEntiteReservee      Define

  HiLink girefFloat               Float
  HiLink girefInclude             Include
  HiLink girefError               Error
  HiLink girefParentOpenError     Error
  HiLink girefParentCloseError    Error
  HiLink girefBooleen             Define
  HiLink girefSpecialVar          Define
  HiLink girefArgumentSolveur     Define
  HiLink girefArgumentExportation Define
  HiLink girefConditionLimite     Define
  HiLink girefOperator            Operator
"  HiLink girefIdentifier      Identifier

  delcommand HiLink
endif
  
let b:current_syntax = "giref"


