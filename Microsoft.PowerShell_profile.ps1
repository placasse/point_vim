# Mon fichier personnel de configuration PowerShell

#oh-my-posh --init --shell pwsh --config ~/.vim/jandedobbeleer.omp.json | Invoke-Expression
oh-my-posh --init --shell pwsh --config ~/.vim/ohmyposh.json | Invoke-Expression

Import-Module PSReadLine

Set-PSReadLineOption -PredictionSource History
Set-PSReadLineOption -PredictionViewStyle ListView
Set-PSReadLineOption -EditMode Windows

