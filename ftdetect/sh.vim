au BufRead,BufNewFile {*.sh}            set cinkeys=0{,0},0),:,!^I,o,O,e 
au BufRead,BufNewFile {*.sh}            set cino=+0,>2
au BufRead,BufNewFile {*.sh}             map <C-C><C-C> :normal 0i#<CR>
au BufRead,BufNewFile {*.sh}             map <C-C><C-D> :s=^\(:space:\)*#=\1=<CR>

