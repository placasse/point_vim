#!/bin/bash
find . -type f -regex '.*\.\(cpp\|cc\|c\|icc\|h\|hpp\|cs\)' -exec grep -a --color -Hn $1 {} \;
