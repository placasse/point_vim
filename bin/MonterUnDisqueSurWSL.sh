#!/bin/bash

DisqueAMonter=$1
PointDeMontage=$2

# Exemple :
# MonterUnDisqueSurWSL.sh \\diskstation\arcane /mnt/x
# Le disque à monter peut être un disque réseau ou un disque dur USB.

if [ -z "$DisqueAMonter" ]
then
  echo DisqueAMonter non renseigné
  exit 1
fi

if [ -z "$PointDeMontage" ]
then
  echo PointDeMontage non renseigné
  exit 1
fi

if [[ -d "$PointDeMontage" ]]
then
  echo "PointDeMontage doit déjà être un répertoire."

sudo mount -t drvfs "$DisqueAMonter" "PointDeMontage"
