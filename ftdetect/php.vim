au BufRead,BufNewFile {*.php}             set cinkeys=0{,0},0),:,!^I,o,O,e,#,;
au BufRead,BufNewFile {*.php}             set indentkeys=0{,0},0),:,!^I,o,O,e,*<Return>,=?>,=<?,=*/
au BufRead,BufNewFile {*.php}             set cino=>2,(0,w1,g0,h2,t0
au BufRead,BufNewFile {*.php}             map <C-C><C-C> :normal 0i//<CR>
au BufRead,BufNewFile {*.php}             map <C-C><C-D> :s=^\(:space:\)*//=\1=<CR>

" Pour probatis
au BufRead,BufNewFile {*.php}             set path=.,/var/www/html/probatis,/usr/share/pear
au BufRead,BufNewFile {*.php}             set encoding=iso-8859-1

let php_htmlInStrings = 1
let php_sql_query = 1
let php_parent_error_close = 1

