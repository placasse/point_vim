#!/bin/bash
#===============================================================================
#
#          FILE:  correct_grammaire.sh
#
#         USAGE:  ./correction_gramaticale.sh
#
#   DESCRIPTION:  utilise languagetool dans bin pour avoir un correcteur
# 		grammatical utilisable sur un fichier latex
#
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:   Corentin Barbu, Patrick Lacasse
#       COMPANY:
#       VERSION:  1.0
#       CREATED:  17/10/2009 14:38:59 CEST
#      REVISION:  ---
#===============================================================================
# Pour utiliser language tool dans vim (sur fichiers latex) : exécuter les lignes de commandes suivantes (en retirant le dièse devant)
#
# Télécharger la dernière version de LanguageTool-XXX.zip autonome.
#
# mv LanguageTool-XXX.zip ~/bin
# cd ~/bin
# unzip LanguageTool-XXX.zip
#
# Rendre ce fichier (~/bin/correction_grammaticale.sh) exécutable
# chmod +x ~/bin/correction_grammaticale.sh
#
# avec bin PATHer dans le .bashrc
#      PATH=$PATH:~/bin
#
# dans le ~/.vimrc copier coller :
# nmap ,c :!echo -e "\n Correction en cours *attendre* que l'écran normal s'affiche\n"<CR>:!correction_grammaticale.sh %<CR>:sp %.LTcheck<CR>gg/Line<CR>w:let numligne = expand("<cword>")<CR>/column<CR>w:let numcolonne = expand("<cword>")<CR>}:wincmd j<CR>:execute ":"numligne<CR>:execute "normal 0"expand(numcolonne)"l"<CR>
# nmap ,n :wincmd k<CR>/Line<CR>w:let numligne = expand("<cword>")<CR>/column<CR>w:let numcolonne = expand("<cword>")<CR>}:wincmd j<CR>:execute ":"numligne<CR>:execute "normal 0"expand(numcolonne)"l"<CR>
#
# Et corriger la ligne suivante pour le numéro de version (et peut-être des règle qui changent).

java -jar ~/bin/LanguageTool-2.8/languagetool-commandline.jar --disable WHITESPACE_RULE,COMMA_PARENTHESIS_WHITESPACE,FRENCH_WHITESPACE,HUNSPELL_NO_SUGGEST_RULE,UNPAIRED_BRACKETS,UPPERCASE_SENTENCE_START,ESPACE_UNITES,HEURES,DOUBLE_PUNCTUATION --language fr --mothertongue fr $1 > $1.LTcheck

exit 0

