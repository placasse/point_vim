#!/bin/bash
#
# Start ssh-agent if it is not running.
# Intended for use on Bash for Windows using the WSL.
#
# Author: Dave Eddy <dave@daveeddy.com>
# Date: October 09, 2017
# License: MIT
#
# Version mofifiée par Patrick Lacasse <patrick.m.lacasse@gmail.com>
# Provient de https://github.com/bahamas10/windows-bash-ssh-agent.git
#

# Could be any file - nothing intrinsically valuable about ~/.ssh/environment
envfile=~/.ssh/environment

# Ensure the environment file exists and has its permissions properly set.
# Source the file - if it was created by this script the source will
# effectively be a noop.
mkdir -p "${envfile%/*}"
touch "$envfile"
chmod 600 "$envfile"
. "$envfile"

# Check if the daemon is already running
if [[ -n $SSH_AGENT_PID ]] && kill -0 "$SSH_AGENT_PID" 2>/dev/null; then
	# The PID is up but it could have been recycled - attempt to list keys.
	# This will exit with 2 if the SSH_AUTH_SOCK is broken.
	ssh-add -l &>/dev/null
	if (($? != 2)); then
		exit 1
	fi
fi

# Overwrite what is in the envfile to start a fresh ssh-agent instance
echo "# Started $(date)" > "$envfile"

# Start ssh-agent.
exec ssh-agent -s >> "$envfile"
