#!/bin/bash

# Copie du .vimrc s'il n'existe pas.
if [ ! -f ~/.vimrc ]; then
        echo Installation du .vimrc
        cp POINTvimrc ~/.vimrc
fi

# Copie du .bashrc s'il n'existe pas.
if [ ! -f ~/.bashrc ]; then
        echo Installation du .bashrc
        cp POINTbashrc ~/.bashrc
fi

# Copie du .gitconfig s'il n'existe pas.
if [ ! -f ~/.gitconfig ]; then
        echo Installation du .gitconfig
        cp POINTgitconfig ~/.gitconfig
fi

# Copie de tout le bin si le répertoire n'existe pas.
if [ ! -d ~/bin ]; then
        echo Installation du répertoire ~/bin
        mkdir ~/bin
        cp bin/* ~/bin
fi

if [ ! -d bundle ]; then
        mkdir -p bundle
        cd bundle
        git clone https://github.com/tpope/vim-fugitive.git
        git clone https://github.com/editorconfig/editorconfig-vim.git
        git clone https://github.com/vim-scripts/The-NERD-tree.git
        git clone https://github.com/rhysd/vim-clang-format.git
        git clone https://github.com/sirtaj/vim-openscad.git
        git clone https://github.com/vim-scripts/ShaderHighLight.git
        git clone https://github.com/martinda/Jenkinsfile-vim-syntax.git
        git clone https://github.com/PProvost/vim-ps1.git
fi
