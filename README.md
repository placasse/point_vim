Mon répertoire .vim .
Contient aussi d'autres fichiers de config à copier ailleurs dans mon ~ .
Exécuter le script suivant pour copier les fichier de config manquant sur le nouveau poste :
``` bash
./installer.sh
```
Les fichiers existants ne seront pas écrasé.

De temps en temps, comparer les fichiers de config pour récupérer les amélioration :
``` bash
vimdiff POINTgitconfig ../.gitconfig
vimdiff POINTbashrc ../.bashrc
vimdiff POINTvimrc ../.vimrc
```

Pour PowerShell, télécharger les fonts https://github.com/ryanoasis/nerd-fonts/releases/download/v2.2.2/CascadiaCode.zip
Installer `Caskaydia Cove Nerd Font Complete Mono Windows Compatible Regular.otf` en cliquant dessus dans le zip.
Configurer le profil PowerShell du terminal pour s'en servir.

```
winget install JanDeDobbeleer.OhMyPosh -s winget
vimdiff Microsoft.PowerShell_profile.ps1 `pwsh -c 'echo $PROFILE'`
```
