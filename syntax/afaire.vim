" Vim syntax file
" Language:	Fichier afaire de probatis
" Maintainer:	Patrick Lacasse <patrick.m.lacasse@gmail.com>
" Last Change:	2009 janvier
"
" This vim syntax file works on vim 5.6, 5.7, 5.8 and 6.x.
" It implements Bram Moolenaar's April 25, 2001 recommendations to make
" the syntax file maximally portable across different versions of vim.


" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syntax match fait    "FAIT - .*"
syntax match non     "NON - .*"
syntax match encours "EN COURS - .*"
syntax match note    "NOTE - .*"
syntax match reponse "REPONSE - .*"
syntax match oblige  "0 - .*"
syntax match cote    "^[1-9] -"

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_afaire_syn_inits")
  if version < 508
    let did_afaire_syn_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink fait      Comment
  HiLink non       Comment
  HiLink encours   Include
  HiLink note      Repeat
  HiLink reponse   String
  HiLink oblige    Error
  HiLink cote      Statement

  delcommand HiLink
endif
  
let b:current_syntax = "afaire"


